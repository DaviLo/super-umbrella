extends Node2D

export(String) var name1 = "Speaker 1"
export(Texture)  var image1

export(String) var name2 = "Speaker 2"
export(Texture)  var image2

export(String, MULTILINE) var text = "Oh \n\n Noes \n\n\n HAHAHA!"
onready var speech_resource = preload("res://hud/speech_boxes/speech_box.tscn")

var pause_manager

var dialogue
var who  = true #who is talking? true = name 1 / false = name 2
var speech_number = 0


func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	assert(finder.has_pauser())
	pause_manager = finder.get_pauser()
	
	#dialogue between two entities is separated with \n\n\n
	dialogue = text.split("\n\n\n")

	$name1/Label.set_text(name1)
	$name1/Sprite.set_texture(image1)

	$name2/Label.set_text(name2)
	$name2/Sprite.set_texture(image2)

	play_dialogue()

func play_dialogue():
	pause_manager.cutscene_pause()
	while speech_number < dialogue.size():

		#swap names
		if who:
			$name1.show()
			$name2.hide()
		else:
			$name1.hide()
			$name2.show()

		who = !who

		#Creates a speech_box
		var speech_box = speech_resource.instance();
		speech_box.new(dialogue[speech_number])

		self.add_child(speech_box)
		yield(speech_box,"finished")

		speech_number = speech_number + 1

	pause_manager.cutscene_pause()
	queue_free()