extends Area2D

const DESTROYABLE = preload("res://entities/components/destroyable/Destroyable.gd")

func _on_body_entered(body):
	for child in body.get_children():
		if child is DESTROYABLE:
			child.destroy()
