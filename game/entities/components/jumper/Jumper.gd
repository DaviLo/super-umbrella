extends Node

export(float) var jump_strength = 750.0 # determines jump height

# Jump multipler
var jump_buff = 1

var entity
var platformer
var control

func _entity_ready(entity):
	self.entity = entity
	self.platformer = entity.get_component(preload("res://entities/components/platformer/Platformer.gd"))
	assert(self.platformer != null)
	self.control = entity.get_component(preload("res://controls/ControlBase.gd"))
	assert(self.control != null)

func _physics_process(delta):
	if self.entity.is_on_floor() and self.control.action_jump:
		self.platformer.push(self.jump_strength * self.jump_buff * Vector2(0, -1))
