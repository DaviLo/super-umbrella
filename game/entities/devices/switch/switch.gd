extends Area2D

export(bool)   var toggle = false
var pressable = true

signal button_press()

func _on_switch_body_entered(body):
	if pressable and body.is_in_group("Player"):
		pressable = false
		
		if toggle:
			
			#Swap frames
			if $Sprite.get_frame() == 0:
				$Sprite.set_frame(1)
			else:
				$Sprite.set_frame(0)
			
			#Delay before being pressable again
			$Timer.start()
			
		else:
			$Sprite.set_frame(1)
		
		emit_signal("button_press")


func _on_Timer_timeout():
	pressable = true
