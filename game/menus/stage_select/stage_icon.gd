extends VBoxContainer

enum { ARROW_LEFT, ARROW_RIGHT }

export(String, FILE, "*.tscn") var stage_scene
export(String) var stage_name
export(String) var flag_required
export(Texture) var icon_texture

var locked = false

signal focused
signal pressed(icon)

func _ready():
	if icon_texture:
		$Button/TextureRect.texture = icon_texture
	$Button/TextureRect.stretch_mode = TextureRect.STRETCH_SCALE
	
	if stage_name:
		$Name.text = stage_name
	else:
		$Name.text = name
	
	var saver = preload("res://managers/saver/saver.gd").new()
	
	if saver.has_save(): 
		var save = saver.load_stage()
		if save.has(stage_name) and save[stage_name] == "compleat":
			self.set_modulate(Color("88de79"))
		elif flag_required != null and not save.has("flag_"+flag_required):
			self.set_modulate(Color("fc1f1f"))
			locked = true

func hide_arrow(direction):
	var arrow
	if direction == ARROW_LEFT:
		arrow = $Button/Arrows/Left
	else:
		arrow = $Button/Arrows/Right
	arrow.modulate = Color(1, 1, 1, 0)


func _on_Button_focus_entered():
	$Name.modulate = Color(1, 1, 1)
	$Button/Arrows.show()
	emit_signal("focused", self)
	


func _on_Button_focus_exited():
	$Name.modulate = Color(.5, .5, .5)
	$Button/Arrows.hide()


func _on_Button_pressed():
	if not locked:
		emit_signal("pressed", self)
	else:
		print("stage is locked!")
